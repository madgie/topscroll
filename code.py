from window import *
from manager import *


a = PygameWrapper()
m = GameManager(SCRN_WDTH, SCRN_HGHT)
sound = SoundManager()

sound.load_sound('objects/sounds/beep.wav', 'beep')
sound.load_sound('objects/sounds/main.wav', 'main')

a.create_window()
a.set_font('objects/fonts/arcade.ttf', 25)

menu_answer = a.menu_loop(m.event_handler, sound)
if menu_answer == 0:
	a.game_loop(m.event_handler)
elif menu_answer == 1:
	pass
