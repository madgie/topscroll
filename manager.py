import pygame.locals


class Pool(object):
	''' 
	element = (Object, Lives, Position X, Position Y, IsAttack)
	'''
	def __init__(self, min_size = 1, max_size = 100):
		self.__depot = list()
		self._min = min_size
		self._max = max_size
	
	def cleaner(self):
		for element in self.__depot:
			if element[1] == 0:
				element = ('Empty', '', '', '', '')

	def add(self, object, lives, x, y):
		overstack = True
		for element in self.__depot:
			if element[0] == 'Empty':
				element = (object, lives, x, y, False)
				overstack = False
		if overstack == True:
			if len(self.__depot) < max_size:
				self.__depot += [(object, lives, x, y, False)]
			if len(self.__depot) == max_size:
				raise ValueError('pool size is over!')



class GameManager(object):
	def __init__(self, screen_width, screen_height):
		self.__score = 0
		self.__player_lives = 0
		self.__screen_width = screen_width
		self.__screen_height = screen_height
		self.__hud_recive = True

	def event_handler(self):
		for e in pygame.event.get():
			if e.type == pygame.QUIT:
				return 'quit'
		
			keys = pygame.key.get_pressed()
			if keys[pygame.K_LEFT] or keys[pygame.K_a]:
				return 'left'
			elif keys[pygame.K_RIGHT] or keys[pygame.K_d]:
			 	return 'right'
			elif keys[pygame.K_UP] or keys[pygame.K_w]:
			 	return 'up'
			elif keys[pygame.K_DOWN] or keys[pygame.K_s]:
				return 'down'
			elif keys[pygame.K_SPACE]:
				return 'space'
			elif keys[pygame.K_ESCAPE]:
				return 'quit'
			else:
				return ''
		return ''

	def game_controller(self):
		pass

	def hud_controller(self):
		if self.__hud_recive == False:
			return None
		else:
			return (self.__player_lives, self.__score)

	def background_controller(self):
		pass


class SoundManager(object):
	def __init__(self):
		self.sound_collection = dict()
		pygame.mixer.init()

	def __del__(self):
		pygame.mixer.quit()

	def load_sound(self, sound, tag):
		self.sound_collection.update({tag : pygame.mixer.Sound(sound)})

	def play_sound(self, tag, times = 0):
		self.sound_collection.get(tag).play(times)

	def stop_sound(self, tag):
		self.sound_collection.get(tag).stop()


#pygame.draw.line(screen, (255, 50, 10), (0, self.__screen_height - 50), (self.__screen_width, self.__screen_height - 50))
