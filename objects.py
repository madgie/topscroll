import pygame.locals



class GameObject(object):
	def __init__(self, x, y, livetime):
		self.x = x
		self.y = y
		self.is_alive = True
		self.livetime = livetime
		self.blink_status = True

	def blink(self, rate):
		if self.__surface.get_alpha() == 255:
			self.blink_status = True
		elif self.__surface.get_alpha() <= 20:
			self.blink_status = False
		
		if self.blink_status == False:
			self.__surface.set_alpha(self.__surface.get_alpha() + rate)
		elif self.blink_status == True:
			self.__surface.set_alpha(self.__surface.get_alpha() - rate)

	def refresh(self):
		self.__surface.set_alpha(255)
	
	def set_surface(self, width, height):
		self.__surface = pygame.Surface([width, height])

	def set_surface(self, surface):
		self.__surface = surface
	
	def set_rect(self, x, y, width, height):
		self.__rect = pygame.Rect(x, y, width, height)

	def set_rect(self, rect):
		self.__rect = rect

	def get_surface(self):
		return self.__surface

	def get_rect(self):
		return self.__rect

	def get_position(self):
		return (self.x, self.y)

# Menu objects---------------

class Text(GameObject):
	def __init__(self, x, y, color, fontobj, text):
		GameObject.__init__(self, x, y, -1)
		self.set_surface(fontobj.render(text, 1, color))


class Label(GameObject):
	def __init__(self, x, y, image):
		GameObject.__init__(self, x, y, -1)
		self.set_surface(pygame.image.load(image))
		
#----------------------------
# Game objects---------------

class Dummy(GameObject):
	def __init__(self, x, y, image):
		GameObject.__init__(self, x, y, -1)
		self.set_surface(pygame.image.load(image))
		self.set_rect()

	def move(x, y):
		self.x += x
		self.y += y
		self.__rect.x += x
		self.__rect.y += y


#----------------------------		

# Old or embraced or saved "for good times" code-------

#much smooth, but slow code
#def blink(self):
#		if self.__surface.get_alpha() == 255:
#			self.blink_status = True
#		elif self.__surface.get_alpha() <= 20:
#			self.blink_status = False
#		
#		if self.blink_status == False:
#			self.__surface.set_alpha(self.__surface.get_alpha() + 20)
#		elif self.blink_status == True:
#			self.__surface.set_alpha(self.__surface.get_alpha() - 20)
#
# much antsy/ragged blinker. 
#def blink(self):
#		if self.__surface.get_alpha() <= 0:
#			self.__surface.set_alpha(255)
#		self.__surface.set_alpha(self.__surface.get_alpha() - 10)
#----------------------------