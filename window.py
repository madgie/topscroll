import pygame.locals

import objects


SCRN_WDTH = 400
SCRN_HGHT = 700


class PygameWrapper(object):
	"""Class for window and others staff managments"""
	def __init__(self):
		self.__should_stop = False
		self.__to_game = False

	def __new__(cls):
		if not hasattr(cls, 'instance'):
			cls.instance = super(PygameWrapper, cls).__new__(cls)
		return cls.instance

	def __del__(self):
		pygame.quit()

	def create_window(self, screenwidth = SCRN_WDTH, screenheight = SCRN_HGHT):
		pygame.init()
		self.screen = pygame.display.set_mode([screenwidth, screenheight])
		pygame.display.set_caption('TopScroll')
		self.__clock = pygame.time.Clock()

	def set_font(self, fontname = None, fontsize = 20):
		self.__font = pygame.font.Font(fontname, fontsize)

	def hud_draw(self, HudController):
		ask = HudController()
		if ask == None:
			pass
		else:
			pass

	def game_draw(self, GameController):
		pass

# Loop section -------------------------------------
	def menu_loop(self, EventHandler, SoundManager):
		selector = 0
		#Menu structure.
		title = objects.Label(0, 20, 'objects/imgs/title.png')
		playbtn = objects.Label(20, 220, 'objects/imgs/playbtn.png')
		quitbtn = objects.Label(20, 300, 'objects/imgs/quitbtn.png')
		helptxt = objects.Text(20, 400, (255, 24, 67), self.__font, 'Select what you want,')
		helptxt2 = objects.Text(20, 420, (255, 24, 67), self.__font, 'and press SPACE')
		#---------------
		background = pygame.Surface(self.screen.get_size())
		background = background.convert()

		background.blit(title.get_surface(), title.get_position())
		background.blit(helptxt.get_surface(), helptxt.get_position())
		background.blit(helptxt2.get_surface(), helptxt2.get_position())

		SoundManager.play_sound('main')

		while not self.__to_game:
			key = EventHandler()
			if key == 'quit':
				self.__to_game = True
			elif key == 'up':
				SoundManager.play_sound('beep')
				if selector < 0:
					selector += 1
				else:
					selector = 0
			elif key == 'down':
				SoundManager.play_sound('beep')
				if selector > -1:
					selector -= 1
				else:
					selector = -1
			elif key == 'space':
				if selector == 0:
					SoundManager.stop_sound('main')
					return 0
				if selector == -1:
					SoundManager.stop_sound('main')
					return 1
				
			self.screen.blit(background, (0, 0))
			
			if selector == 0:
				quitbtn.refresh()
				playbtn.blink(20)
			if selector == -1:
				playbtn.refresh()
				quitbtn.blink(20)

			self.screen.blit(playbtn.get_surface().convert(), playbtn.get_position())
			self.screen.blit(quitbtn.get_surface().convert(), quitbtn.get_position())

			pygame.display.flip()
			self.__clock.tick(30)

	
	def game_loop(self, EventHandler, HudController, GameController):
		
		while not self.__should_stop:
			if EventHandler() == 'quit':
				self.__should_stop = True

			self.screen.fill((0, 0, 0))
			self.game_draw(GameController)
			self.hud_draw(HudController)
			pygame.display.flip()
			self.__clock.tick(30)

# --------------------------------------------------